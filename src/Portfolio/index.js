import React, {Component} from 'react';
import Toolbar from '../components/Toolbar'
import SideDrawer from '../components/SideDrawer'
import Backdrop from '../components/Backdrop'
import ProjectGallery from '../components/ProjectGallery'
import { MdKeyboardArrowDown } from 'react-icons/md';

class Portfolio extends Component{
  state = {
    sideDrawerOpen:false
  };

  drawerToggleClickHandler = () => {
    this.setState((prevState) => {
      return {sideDrawerOpen:!prevState.sideDrawerOpen}
    });
  }

  backdropClickHandler = () => {
    this.setState({sideDrawerOpen:false})
  }

  render(){
    let backDrop;

    if(this.state.sideDrawerOpen){
      backDrop   = <Backdrop click={this.backdropClickHandler}/>
    }
    return (
      <div style={{height:'100%'}}>
      <Toolbar drawerClickHandler={this.drawerToggleClickHandler}/>
      <SideDrawer show={this.state.sideDrawerOpen}/>
      {backDrop}
      <main style={{marginTop:'64px',display:'flex',flexDirection:'column',alignItems:'center',justifyContent:'space-between',position:'absolute',top:'100px'}}>
          <h1>portfolio</h1>
          <ProjectGallery title="React" image="https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/create-a-board/2a6cc1bc24e782bb4dd4de4c3120054d/01.png" description="Drello is one version of TRELLO"/>
          <ProjectGallery title="React" image="https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/create-a-board/2a6cc1bc24e782bb4dd4de4c3120054d/01.png" description="Drello is one version of TRELLO"/>
          <ProjectGallery title="React" image="https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/create-a-board/2a6cc1bc24e782bb4dd4de4c3120054d/01.png" description="Drello is one version of TRELLO"/>
      </main>
      </div>
    );
  }

}

export default Portfolio;
