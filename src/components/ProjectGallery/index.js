import React from 'react';
import { Container,Main } from './styles';

function ProjectGallery({title,image,description}){
  return(
    <Container>
      <header><h1 style={{paddingLeft:20}}>{title}</h1></header>
      <Main>
        <img src={image}/>
        <p>{description} </p>
      </Main>
    </Container>
  )
}

export default ProjectGallery;
