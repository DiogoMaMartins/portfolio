import styled, { css } from 'styled-components';


export const Container = styled.div`
  display:flex;
  min-width:90%;
  min-height:300px;
  flex-direction:column;
  margin:10px;
  border-bottom:2px solid #FF4700;
  justify-content:space-around;
  align-items:center;
  header{
    background:#000;
    color:#FF4700;
    top:0;
    left:0;
    width:100%;
    height:60px;
    display:flex;
  }

`;

export const Main = styled.main`
  display:flex;
  flex-direction:column;
img{
  position:relative;
  width:100%;
  margin:0;
  padding:0;
}
p{
  margin:0;

  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 24px;
  color: #fff;
}

`;
