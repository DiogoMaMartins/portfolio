import React from 'react';
import { Container,Form,InputFilds,Button,Input,Message } from './styles';


function ContactForm(){
  return(
    <Container>
      <Form>
        <InputFilds>
          <Input type="text" placeholder="Your Name"/>
          <Input type="text" placeholder="Email Address"/>
          <Input type="text" placeholder="Phone"/>
          <Input type="text" placeholder="Subject"/>
        </InputFilds>
        <Message>
          <textarea placeholder="Message"></textarea>
          <Button>Send</Button>
        </Message>

      </Form>
    </Container>
  )
}

export default ContactForm;
