import styled, { css } from 'styled-components';

export const Container = styled.div`
  position:absolute;
  top:50%;
  transform:translateY(-50%);
  width:100%;
  padding:0 20px;
`;

export const Form = styled.form`
  max-width:550px;
  margin:0 auto;
  background:transparent;
  padding:30px;
  border-radius:5px;
  display:flex;
  box-shadow:0 0 10px #FF4700;

`;


export const InputFilds = styled.div`
  display:flex;
  flex-direction:column;
  margin-right:4%;
  width:50%;
  margin:10px 0;
  background:transparent;
  border:0;

`;

export const Input = styled.input`
margin:10px 0;
background:transparent;
border:0;
border-bottom:2px solid #fff;
padding:10px;
color:#FF4700;
font-size:25px;
font-family:Roboto;

::-webkit-input-placeholder{
  color:#FF4700;
}
::-moz-input-placeholder{
  color:#FF4700;
}

`;

export const Message = styled.div`
  width:48%;
  margin-left:10px;
  textarea{
    margin:10px 0;
    background:transparent;
    border:0;
    border-bottom:2px solid #fff;
    width:100%;
    color:#FF4700;
    height:194px;
    font-size:25px;
    font-family:Roboto;

    ::-webkit-input-placeholder{
      color:#FF4700;

    }
    ::-moz-input-placeholder{
      color:#FF4700;
    }
  }
`;



export const Button = styled.button`
  background:#000;
  text-align:center;
  padding:15px;
  border-radius:5px;
  width:100%;
  color:#fff;
  cursor:pointer;
  font-size:25px;
  font-family:Roboto;
  text-transform:uppercase;
`;
