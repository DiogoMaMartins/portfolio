import styled, { css } from 'styled-components';
import React from 'react';
import { NavLink } from 'react-router-dom'

export const StyledLink = styled(NavLink)`
`;

export const Span = styled.span`

  color:white;
  display:none;
  margin-top:4px;
  height:4px;
  ${({ active }) => active && `
  display:flex;
  background:#FF4700;
 `}
`;


export const Toolbarr = styled.header`
  position:absolute;
  top:0;
  left:0;
  width:100%;
  background-color:#181818;
  height:105px;
  z-index:200;
`;

export const MenuBtn = styled.div`
  display:block;
  @media (min-width: 768px) {
    display:none;
  }
`;


export const Nav = styled.nav`
  display:flex;
  height:100%;
  align-items:center;
  padding: 0 1rem;

`;

export const Logo = styled.div`

@media (min-width:769px){

  position: absolute;
  padding:20;
  width: 82px;
  height: 82px;
  display:flex;
  justify-content:center;
  align-items:center;
  left: 52px;
  border-radius:50%;
  background: #000000;
  color:white;
  border: 6px solid #FF4700;

}
@media (max-width: 768px) {

  position: absolute;
  padding:20;
  width: 82px;
  height: 82px;
  display:flex;
  justify-content:center;
  align-items:center;
  right: 50px;
  top:10px;
  border-radius:50%;
  background: #000000;
  color:white;
  border: 6px solid #FF4700;

}
`;

export const TxtLogo = styled.p`
font-family: Rock Salt;
font-style: normal;
font-weight: normal;
font-size: 36px;
line-height: 80px;
color: #FFFFFF;
`;

export const Spacer = styled.div`
  flex:1;
`;
  export const Pages = styled.div`
  @media (max-width: 768px) {
    display:none;
  }
  a{
    color:white;
    text-decoration:none;
  }
  li{
    padding:0 0.5rem;
    font-family: Roboto;
    font-style: normal;
    font-weight: normal;
    font-size: 30px;
    line-height: 36px;
    color: #FFFFFF;
    text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  }
    ul{
      list-style:none;
      margin:0;
      padding:0;
      display:flex;
    }
    a:hover,a:active{
      color:red;
    }
  `;
