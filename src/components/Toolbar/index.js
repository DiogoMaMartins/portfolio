import React from 'react';
import {TxtLogo, Toolbarr,Nav,Logo,Pages,Spacer,MenuBtn,StyledLink,Span } from './styles';
import DrawerToggleButton from '../SideDrawer/DrawerToggleButton';
import SideDrawer from '../SideDrawer';

function Toolbar({drawerClickHandler}) {
  return (
    <Toolbarr>
      <Nav>
        <MenuBtn>
      <DrawerToggleButton click={drawerClickHandler}/>

        </MenuBtn>
        <Logo><TxtLogo>DM</TxtLogo></Logo>
        <Spacer/>
        <Pages>
          <ul>
            <li><StyledLink to="/">Home<Span active></Span></StyledLink></li>
            <li><StyledLink to="/portfolio">Portfolio<Span ></Span></StyledLink></li>
            <li><StyledLink to="/contact" >Contact<Span ></Span></StyledLink></li>
          </ul>
        </Pages>
      </Nav>
    </Toolbarr>
  );
}

export default Toolbar;
