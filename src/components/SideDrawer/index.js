import React from 'react';
import { Nav,SocialNetwork } from './styles.js';
import { MdKeyboardArrowDown } from 'react-icons/md';
import instagram from './instagram.svg';
import github from './github.svg';
import bitbucket from './bitbucket.svg';
import { NavLink } from 'react-router-dom'
function SideDrawer({show}){
  let drawerClasses = false;
  if (show){
    drawerClasses = true;
  }
  return(
    <div>
    <Nav active={drawerClasses} >
    <ul>
      <li><NavLink to="/">Home</NavLink></li>
      <li><NavLink to="/portfolio">Portfolio</NavLink></li>
      <li><NavLink to="/contact">Contact</NavLink></li>
      <ul style={{display:'flex',flexDirection:'row',background:'white',borderWidth:1,borderColor:'red',height:40}}>
          <li style={{marginTop:'6px'}}><a href="https://www.instagram.com/who_martins/"><img style={{width:40,height:40}} src={instagram} /></a></li>
            <li style={{marginTop:'6px'}}><a href="https://github.com/DiogoMaMartins"><img style={{width:40,height:40}} src={github} /></a></li>
            <li style={{marginTop:'6px'}}><a href="https://bitbucket.org/%7B1d2bc940-5508-4d77-b8b2-c83c69d1618b%7D/"><img style={{width:40,height:40}} src={bitbucket} /></a></li>
      </ul>
    </ul>
    </Nav>
    </div>
  )

}

export default SideDrawer;
