import React from 'react';
import { Button,Line } from './styles';

function DrawerToggleButton({click}){
  return(
    <Button onClick={click} >
      <Line/>
      <Line/>
      <Line/>
    </Button>
  )

}

export default DrawerToggleButton;
