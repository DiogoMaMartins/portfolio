import styled, { css } from 'styled-components';

export const Button = styled.div`
  display:flex;
  flex-direction:column;
  justify-content:space-around;
  height:24px;
  width:30px;
  background:transparent;
  border:none;
  cursor:pointer;
  padding:0;
  box-sizing:border-box;

  :focus {
    outline:none;
  }
`;

export const Line = styled.div`
  width:30px;
  height:1px;
  background:white;
`;


export const SocialNetwork = styled.ul`
  background:white;
  border-bottom:1px solid:blue;
  height:20px;
  width:90%;
  list-style:none;
  display:flex;
  flex-direction:rown;
  justify-content:space-around;
  align-items:center;
  li{

  }
`;

export const Nav = styled.nav`
  height:40%;
  background:#060606;
  box-shadow:1px 0px 7px #060606;
  position:fixed;
  width:100%;
  top:0;
  left:0;
  z-index:200;
  transform:translatex(-110%);
  transition:transform 0.3s ease-out;

  ${({ active }) => active && `
    transform:translatex(0%);
 `}

  ul{
    height:100%;
    min-width:90%;
    list-style:none;
    display:flex;
    flex-direction:column;
    justify-content:space-around;
    align-items:center;
  }
  li a{
    padding:0 0.5rem;
    font-family: Roboto;
    font-style: normal;
    font-weight: normal;
    font-size: 30px;
    line-height: 36px;
    color: #FF4700;
    text-decoration:none;
    display:flex;
    text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  }


  a:hover,a:active{
    color:white;
  }
`;
