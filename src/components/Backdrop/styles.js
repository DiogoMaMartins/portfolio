import styled, { css } from 'styled-components';


export const Backdrops = styled.div`
  position:fixed;
  width:100%;
  height:100%;
  top:0;
  left:0;
  background:transparent;
  z-index:100;
`;
