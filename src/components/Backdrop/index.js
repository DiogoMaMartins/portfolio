import React from 'react';

import {Backdrops} from './styles';
function Backdrop({click}){
  return  <Backdrops onClick={click}/>
}

export default Backdrop;
