import styled, { css } from 'styled-components';

export const Container = styled.div`
  display:flex;
  width:100%;
  background-color:#121212;
  flex-direction:row;
  justify-content:space-around;
  align-items:center;

  ul,li{
    text-decoration:none;
    list-style:none;
    display:flex;
    flex-direction:row;
    justify-content:space-around;
    align-items:center;
    width:40%;
     height:130;

  }
`
export const AboutMe = styled.div`
  margin-left:100px;
  display:flex;
  flex-direction:column;
  background:transparent;
  max-width:40%;
  min-height:300px;

  p{
    font-size:25px
  }
`;
