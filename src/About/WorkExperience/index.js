import React from 'react';
import { Container,AboutMe } from './styles';

import instagram from '../../img/socialNetwork/instagram.svg';
import github from '../../img/socialNetwork/github.svg';
import bitbucket from '../../img/socialNetwork/bitbucket.svg';

function WorkExperience(){
  return(
    <Container>
      <AboutMe>
        <h1>Hi, I'm Diogo Martins a full-stack developer.</h1>
        <p>I graduated as a developer React mobile and web applications at 	Becode. I did my internship at the company Bit4you where I learn Vuejs, Blockchain, and Golang. If you have interested or your company feels free to contact me.</p>
      </AboutMe>
      <ul>
          <li style={{marginTop:'6px'}}><a href="https://www.instagram.com/who_martins/"><img style={{width:100,height:100}} src={instagram} /></a></li>
            <li style={{marginTop:'6px'}}><a href="https://github.com/DiogoMaMartins"><img style={{width:100,height:100}} src={github} /></a></li>
            <li style={{marginTop:'6px'}}><a href="https://bitbucket.org/%7B1d2bc940-5508-4d77-b8b2-c83c69d1618b%7D/"><img style={{width:100,height:100}} src={bitbucket} /></a></li>
      </ul>
    </Container>
  )
}

export default WorkExperience;
