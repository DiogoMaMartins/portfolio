import React from 'react';
import { Container,Image,BasicInformation,LeftSide,Information,RightSide } from './styles'
import { MdEmail,MdStreetview,MdViewAgenda,MdFlag,MdPhoneIphone } from 'react-icons/md';
function PersonalInformation(){
  return(
    <Container>
        <LeftSide>
          <Image src="https://instagram.fbru2-1.fna.fbcdn.net/vp/ee9f2420e1be781287ed7e864e0f64c9/5DD922FE/t51.2885-19/s150x150/64792433_361886291179110_48267370753425408_n.jpg?_nc_ht=instagram.fbru2-1.fna.fbcdn.net"/>
          <Information>
            <h1>Diogo Martins</h1>
            <h2>Junior Full Stack Developper</h2>
          </Information>
        </LeftSide>
        <RightSide>
         <Information>
          <p>  <MdEmail size={20} color="#FF4700"   style={{marginBottom:'-8px'}}/>mamartinsdiogo@gmail.com</p>
          <p><MdStreetview size={20} color="#FF4700" style={{marginBottom:'-8px'}}/>Avenue Gounod 1070 Anderlecht Belgium</p>
          <p><MdViewAgenda size={20} color="#FF4700" style={{marginBottom:'-8px'}}/>24 years old</p>
          <p><MdFlag size={20} color="#FF4700" style={{marginBottom:'-8px'}}/>Portuguese</p>
          <p><MdPhoneIphone size={20} color="#FF4700" style={{marginBottom:'-8px'}}/>+32499506406</p>
          </Information>
        </RightSide>

    </Container>
  )

}

export default PersonalInformation;
