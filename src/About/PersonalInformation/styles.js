import styled, { css } from 'styled-components';


export const Container = styled.div`
  display:flex;
  flex-direction:row;
  width:100%;
  background-color:#181818;
  justify-content:space-around;
`
;

export const Image = styled.img`
  width:200px;
  height:200px;
  border:2px solid #FF4700;
`;

export const BasicInformation = styled.div`
  display:flex;
  flex-direction:row;
  justify-content:space-between;
  align-items:space-around;
  padding:20px;
`;

export const LeftSide = styled.div`
  display:flex;
  flexDirection:row;
`;
export const Information = styled.div`
  display:flex;
  flex-direction:column;
  margin-left:10px;

`;

export const RightSide = styled.div`
  display:flex;
  flexDirection:row;
  float:right;
`;
