import styled, { css } from 'styled-components';


export const Container = styled.div`
  display:flex;
  width:100%;
  background-color:#181818;
  flex-direction:column;
  justify-content:space-around;
`
;

export const Images = styled.div`
  display:flex;
  width:100%;
  background-color:transparent;
  justify-content:space-around;
  margin:20px;
`
;


export const Image = styled.img`
  height:100px;
  width:100px;

`;
