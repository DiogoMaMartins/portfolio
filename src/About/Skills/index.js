import React from 'react';

import { Container,Image,Images } from './styles';

function Skills(){
  return(
    <Container>
    <Images>
    <Image src={require("../../img/Languages/html.png")}/>
    <Image src={require("../../img/Languages/css.png")}/>
    <Image src={require("../../img/Languages/javascript.png")}/>
    <Image src={require("../../img/Languages/nodejs.png")}/>
    <Image src={require("../../img/Languages/react.png")}/>
    </Images>
    <Images>
    <Image src={require("../../img/Languages/vuejs.png")}/>
    <Image src={require("../../img/Languages/golang.png")}/>
    <Image src={require("../../img/Languages/figma.png")}/>
    <Image src={require("../../img/Languages/postman.png")}/>
    <Image src={require("../../img/Languages/docker.png")}/>
    </Images>
    </Container>
  )
}

export default Skills;
