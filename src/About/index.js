import React, {Component} from 'react';
import Toolbar from '../components/Toolbar'
import SideDrawer from '../components/SideDrawer'
import Backdrop from '../components/Backdrop'
import { MdKeyboardArrowDown } from 'react-icons/md';

import PersonalInformation from './PersonalInformation';
import Skills from './Skills';
import WorkExperience from './WorkExperience';
class About extends Component{
  state = {
    sideDrawerOpen:false
  };

  drawerToggleClickHandler = () => {
    this.setState((prevState) => {
      return {sideDrawerOpen:!prevState.sideDrawerOpen}
    });
  }

  backdropClickHandler = () => {
    this.setState({sideDrawerOpen:false})
  }

  render(){
    let backDrop;

    if(this.state.sideDrawerOpen){
      backDrop   = <Backdrop click={this.backdropClickHandler}/>
    }
    return (
      <div style={{height:'100%'}}>
      <Toolbar drawerClickHandler={this.drawerToggleClickHandler}/>
      <SideDrawer show={this.state.sideDrawerOpen}/>
      {backDrop}
      <main style={{width:'100%',display:'flex',height:'100vh',flexDirection:'column',alignItems:'center',justifyContent:'center',top:'200px',position:'absolute'}}>
        <PersonalInformation/>
        <WorkExperience/>
        <Skills/>
      </main>
      </div>
    );
  }

}

export default About;
