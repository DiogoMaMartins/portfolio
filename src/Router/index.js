import React, { Component } from 'react';

import { BrowserRouter,Route,Switch,Redirect } from 'react-router-dom';

import Home from '../Home';
import Portfolio from '../Portfolio';
import Contact from '../Contact';
import About from '../About';

class Routes extends Component {
	render(){
		return(
		<BrowserRouter>
			<Switch>
				<Route exact path='/' component={Home}/>
				<Route exact path='/portfolio' component={Portfolio}/>
				<Route exact path='/about' component={About}/>
				<Route exact path='/contact' component={Contact}/>
			</Switch>
		</BrowserRouter>
		);
	}
}

export default Routes;
