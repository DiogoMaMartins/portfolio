import React, {Component} from 'react';
import Toolbar from '../components/Toolbar'
import SideDrawer from '../components/SideDrawer'
import Backdrop from '../components/Backdrop'
import { MdKeyboardArrowDown } from 'react-icons/md';
import { NavLink } from 'react-router-dom'

class Home extends Component{
  state = {
    sideDrawerOpen:false
  };

  drawerToggleClickHandler = () => {
    this.setState((prevState) => {
      return {sideDrawerOpen:!prevState.sideDrawerOpen}
    });
  }

  backdropClickHandler = () => {
    this.setState({sideDrawerOpen:false})
  }

  render(){
    let backDrop;

    if(this.state.sideDrawerOpen){
      backDrop   = <Backdrop click={this.backdropClickHandler}/>
    }
    return (
      <div style={{height:'100%'}}>
      <Toolbar drawerClickHandler={this.drawerToggleClickHandler}/>
      <SideDrawer show={this.state.sideDrawerOpen}/>
      {backDrop}
      <main style={{width:'100%',display:'flex',height:'100vh',flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
        <h1 style={{color:'#FF4700',textAlign:'center',fontFamily:'Roboto',fontSize:'60px'}}>Junior Full Stack Developer</h1>
        <p style={{color:'#fff',textAlign:'center',fontFamily:'Roboto',fontSize:'30px'}}>Welcome to my world!I am open to receive feedbacks</p>
        <NavLink to="/about" style={{position:'absolute',bottom:'-40px',alignSelf:'center'}}><MdKeyboardArrowDown size={100} color="#FF4700"  /></NavLink>
        
      </main>
      </div>
    );
  }

}

export default Home;
